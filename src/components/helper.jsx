
import { create } from 'apisauce';
import { addToast } from './toaster';

const getAccessToken = () => {
    const authUser = JSON.parse(localStorage.getItem('authUser'));
    const accessToken = authUser ? authUser['accessToken'] : null
    return accessToken
}

const CreateApi = () => {
    const api = create({
        baseURL: import.meta.env.VITE_USERLIST_API,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + getAccessToken(),
        },
    })
    return api
}


const getUsersList = async () => {
    const api = CreateApi();
    try {
        const response = await api.get('getUsersList');
        if (response.status >= 200 && response.status < 300) {
            return response.data;
        } else {
            throw new Error('Backend server error');
        }
    } catch (error) {
        if (error.response) {
            console.error('HTTP error:', error.response.data);
        } else if (error.request) {
            console.error('No response from the server');
        } else {
            console.error('Error:', error.message);
        }
        return [{ error: error.message }];
    }
};

const postLogin = async (data) => {
    const api = CreateApi();
    try {
        const response = await api.post('login', data);
        if (response.status >= 200 && response.status < 300) {
            return response.data;
        } else {
            throw new Error('HTTP error');
        }
    } catch (error) {
        if (error.response) {
            console.error('HTTP error:', error.response.data);
        } else if (error.request) {
            console.error('No response from the server');
        } else {
            console.error('Error:', error.message);
        }
        return null;
    }
};

const postNewUser = async (data) => {
    const api = CreateApi();
    try {
        const response = await api.post('adduser', data);
        if (response.status >= 200 && response.status < 300) {
            addToast("New user created!", 'success');
            return response.data;
        } else {
            addToast("Error creating user", 'danger');
            throw new Error('HTTP error');
        }
    } catch (error) {
        if (error.response) {
            console.error('HTTP error:', error.response.data);
        } else if (error.request) {
            console.error('No response from the server');
        } else {
            console.error('Error:', error.message);
        }
        return null;
    }
};

const postUserUpdate = async (data) => {
    const api = CreateApi();
    try {
        const response = await api.post('updateuser', data);
        if (response.status >= 200 && response.status < 300) {
            return response.data;
        } else {
            throw new Error('HTTP error');
        }
    } catch (error) {
        if (error.response) {
            console.error('HTTP error:', error.response.data);
        } else if (error.request) {
            console.error('No response from the server');
        } else {
            console.error('Error:', error.message);
        }
        return null;
    }
};

export { postLogin, getUsersList, postNewUser, postUserUpdate };