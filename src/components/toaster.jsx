import React, { useEffect, useState, useRef } from 'react';
import mitt from 'mitt';
import { createUseStyles } from 'react-jss';

const useStyles = createUseStyles({
  toastContainer: {
    display: 'grid',
    gap: '10px',
    width: 'max-content',
    height: 'auto',
    position: 'absolute',
    top: '0px',
    right: '0px',
    gridTemplateColumns: '1fr',
    justifyItems: 'end',
  },
  toast: {

    width: 'max-content',
    minWidth: '250px',
    height: '30px',
    padding: '1em',
    fontSize: '1.2em',
    lineHeight: '1.1em',
    fontWeight: '600',
    color: 'white',
  },
  success: { backgroundColor: 'rgba(45, 208, 28, 1)', },
  info: { backgroundColor: 'rgba(28, 75, 208)', },
  warning: { backgroundColor: 'rgba(208, 193, 28, 1)', },
  error: { backgroundColor: 'rgba(208, 28, 31, 1)', }
});

const emitter = mitt();

const addToast = (text, type) => {
  emitter.emit('addToast', { text, type });
};

const clearToast = () => {
  emitter.emit('clearToasts');
};

const ToastContainer = ({ toasts }) => {
  const classes = useStyles();
  return (
    <div className={classes.toastContainer}>
      {toasts.map((toast) => (
                  <div key={toast.id} className={`${classes.toast} ${classes[toast.type]}`}>
          {toast.text}
        </div>
      ))}
    </div>
  );
};

const ToastProvider = ({ children }) => {
  const [toasts, setToasts] = useState([]);
  const toastIdCounterRef = useRef(1); 


  useEffect(() => {
    const addToastListener = ({ text, type }) => {
      const id = toastIdCounterRef.current++;
      const newToast = { id, text, type };
      setToasts((prevToasts) => [newToast, ...prevToasts]);
      
      setTimeout(() => {
        setToasts((prevToasts) => prevToasts.filter((toast) => toast.id !== id));
      }, 3000);
    };

    const clearToastListener = () => {
      setToasts([]);
    };

    emitter.on('addToast', addToastListener);
    emitter.on('clearToasts', clearToastListener);

    return () => {
      emitter.off('addToast', addToastListener);
      emitter.off('clearToasts', clearToastListener);
    };
  }, []);

  return (
    <>
      {children}
      <ToastContainer toasts={toasts} />
    </>
  );
};

export { addToast, clearToast, ToastProvider };
