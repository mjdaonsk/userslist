import { useEffect, useState, useRef } from 'react'
import { createUseStyles } from 'react-jss'
import countriesList from './components/countriesList'
import { postLogin, getUsersList, postNewUser, postUserUpdate } from './components/helper'
import classNames from 'classnames';
import { addToast } from './components/toaster';

import './App.css'

const useStyles = createUseStyles({
  usersListConatainer: {
    display: 'grid',
    gridAutoColumns: '1fr',
    gridAutoRows: '1fr',
  },
  loginRow: {
    display: 'grid',
    gridTemplateColumns: '1fr 1fr',
    gap: '1em',
    gridAutoFlow: 'row',
    gridTemplateAreas: ". .",
    textAlign: 'start',
    padding: '1em',
    '& input': {
      padding: '0.5em',
      border: '1px solid gray',
      outline: 'none',
      borderRadius: '0px',
      fontSize: '1.1em',
      '&::placeholder': {
        color: 'gray',
      },
    },
  },
  usersListUserRow: {
    display: 'grid',
    gridTemplateColumns: '2fr 2fr 1fr  2fr',
    gap: '1em',
    gridAutoFlow: 'row',
    gridTemplateAreas: ". . . .",
    textAlign: 'start',
    padding: '1em',
    '&:nth-child(odd)': {
      backgroundColor: 'rgba(208, 28, 31, 0.1)',
    },
    '&:hover': {
      backgroundColor: 'rgba(208, 28, 31, 0.2)',
    },
    '&:first-child': {
      backgroundColor: 'rgba(208, 28, 31, 1)',
      fontWeight: 600,
      color: 'white',
      cursor: 'pointer',
    },
    '& input': {
      padding: '0.5em',
      border: '1px solid gray',
      outline: 'none',
      borderRadius: '0px',
      fontSize: '1.1em',
      '&::placeholder': {
        color: 'gray',
      },
    },
    '& span': {
      marginLeft: '1em',
    },
  },
  defectedRow: {
    backgroundColor: 'rgba(208, 28, 31, 1) !important',
  },
  active: {
    fontWeight: 900,
    fontSize: '1.3em',
  },
  inactive: {
    fontWeight: 200,
    fontSize: '1.3em',
  },
  button: {
    display: 'inline-block',
    width: '7em',
    height: '1.5em',
    backgroundColor: 'rgba(208, 28, 31, 1)',
    color: 'white',
    padding: '0.5em',
    fontSize: '1.3em',
    lineHeight: '1em',
    marginBottom: '1em',
    marginRight: '1em',
    cursor: 'pointer',
    '& img': {
      width: '1em',
      height: '1em',
      marginRight: '1em',
      transform: 'translateY(0.2em)',
    },
    '&:hover': {
      backgroundColor: 'rgba(208, 28, 31, 0.8)',
    },

  },
  dNone: {
    display: 'none',
  },
  disabled: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    cursor: 'default',
    '&:hover': {
      backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
  }
})


function App() {
  const classes = useStyles()
  const [usersList, setUsersList] = useState([]) // list of users to display
  const [activeSort, setActiveSort] = useState('');  // to display current sorting
  const [sortOrder, setSortOrder] = useState('asc'); // to display current sorting
  const [newUser, setNewUser] = useState({ username: '', email: '', age: '', country: '' }) // object for new user
  const [isNewUser, setIsNewUser] = useState(false) // If we are in a proccess of new user creation
  const [isAuthUser, setIsAuthUser] = useState(false) // If user is logged in
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState(''); // Credentials
  const [isNewUserDisabled, setIsNewUserDisabled] = useState(true);
  const [newUserWarning, setNewUserWarning] = useState('');
  const loginInputRef = useRef(null);

  // Initial update of usersList and selection of interface (auth / non auth)
  useEffect(() => {
    let ignore = false;
    const authUser = JSON.parse(localStorage.getItem('authUser'));
    if (authUser) {
      getUsersList().then(json => {
        if (!ignore) {
          if (json[0].error) {
            console.log(json[0].error)
            setIsAuthUser(false)
            loginInputRef.current.focus();
          } else {
            setUsersList(json)
            setIsAuthUser(true)
          }
        }
      });
    } else {
      setIsAuthUser(false)
      loginInputRef.current.focus();
    }
    return () => {
      ignore = true;
    };
  }, []);


  // Function to login
  const handleLogin = () => {

    postLogin({ username: username, password: password }).then(json => {
      if (json.error) {
        addToast("Authentication error", 'error');
        setIsAuthUser(false)
      } else {
        addToast("Authentication successful", 'success');
        localStorage.setItem('authUser', JSON.stringify({ username: 'admin', accessToken: json.accessToken }));
        setIsAuthUser(true)
      }
    });
  };

  // Monitor success login to update usersList and interface
  useEffect(() => {
    // console.log(usersList)
    if (isAuthUser) {
      getUsersList().then(json => {
        if (json[0].error) {
          console.log(json[0].error)
          setIsAuthUser(false)
          addToast("Authentication error, logging out", 'warning');
        } else {
          setUsersList(json)
        }
      });
    }
    return () => {
      null
    }
  }, [isAuthUser])


  // Function to sort usersList by given column and order
  const handleSort = (sortBy, sort) => {

    // I was thinking about increasing performance by caching sorting result, but didn't found safe solution. 
    const sortedUsers = [...usersList].sort((a, b) => {
      return sort === 'asc' ? (a[sortBy] < b[sortBy] ? -1 : 1) : a[sortBy] > b[sortBy] ? -1 : 1;
    });

    // Set ActiveSort & Order to display current column and type of sorting
    setActiveSort(sortBy);
    setSortOrder(sort)
    setUsersList(sortedUsers);

  };


  const validateUpdatedUser = (updatedUser, i) => {

    const usernamePattern = /^[a-zA-Z\s]{1,60}$/;
    const isUsernameValid = usernamePattern.test(updatedUser.username);
    if (!isUsernameValid) { addToast("Pattern is a-zA-Z[1,60]", 'warning'); }

    const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    const isEmailValid = emailPattern.test(updatedUser.email);
    if (!isEmailValid) { addToast("Must be a valid email", 'warning'); }

    const agePattern = /^[0-9]{1,3}$/;
    const isAgeValid = agePattern.test(updatedUser.age);
    if (!isAgeValid) { addToast("Age must be 0-999", 'warning'); }

    const isCountryValid = countriesList.some((country) => country.name === updatedUser.country);
    if (!isCountryValid) { addToast("Choose country from the list", 'warning'); }

    if (isUsernameValid && isEmailValid && isAgeValid && isCountryValid) {
      const row = document.getElementById(`ur-${i}`);
      row.classList.remove(classes.defectedRow);
      setNewUserWarning('');
      postUserUpdate(updatedUser)
    } else {
      const row = document.getElementById(`ur-${i}`);
      row.classList.add(classes.defectedRow);
      setNewUserWarning('User data is invalid');
    }
  }

  // Function to update user data in state on edit. 
  const handleUpdate = (i, type, value) => {
    const updatedUser = { ...usersList[i], [type]: value };
    setUsersList((current) =>
      current.map((obj, index) => {
        if (index === i) {
          return { ...obj, [type]: value };
        }
        return obj;
      }),
    );
    validateUpdatedUser(updatedUser, i)
  }

  // Function to filter countries for autocomplete
  const handleFilterCountries = (inputValue) => {

    const filteredCountries = countriesList.filter((country) =>
      country.name.toLowerCase().startsWith(inputValue.toLowerCase())
    );

    return filteredCountries;
  };

  // Function to start and cancel new user process 
  const handleNewUser = (status) => {
    setIsNewUser(!isNewUser)
    if (!status) {
      setNewUser({ username: '', email: '', age: '', country: '' })
      setNewUserWarning('');
      setIsNewUserDisabled(true)
    }
  }

  // Function to handle editing new user credentials
  const handleNewUserUpdate = (type, value) => {
    setNewUser((current) => ({
      ...current,
      [type]: value,
    }));
  }

  // Function to write new user to database
  const createNewUser = async () => {
    const result = await postNewUser({ username: newUser.username, email: newUser.email, age: newUser.age, country: newUser.country })
    setIsNewUserDisabled(true)
    if (result.message === 'User added successfully') {
      setNewUserWarning('');
      handleNewUser(false)
      getUsersList().then(json => {
        if (json[0].error) {
          setIsAuthUser(false)
        } else {
          setUsersList(json)
        }
      });
    } else {
      setNewUserWarning('User data is invalid');
    }
  }

  // Damned button) for new user creation. Disabled if data is invalid.  
  const combinedClasses = classNames({
    [classes.button]: isNewUser,
    [classes.dNone]: !isNewUser,
    [classes.disabled]: isNewUserDisabled,
  });


  // Validate data of new user and enable / disable submit button
  const validateData = () => {

    const usernamePattern = /^[a-zA-Z\s]{1,60}$/;
    const isUsernameValid = usernamePattern.test(newUser.username);

    const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    const isEmailValid = emailPattern.test(newUser.email);

    const agePattern = /^[0-9]{1,3}$/;
    const isAgeValid = agePattern.test(newUser.age);

    const isCountryValid = countriesList.some((country) => country.name === newUser.country);

    if (isUsernameValid && isEmailValid && isAgeValid && isCountryValid) {
      setIsNewUserDisabled(false);
      setNewUserWarning('');
    } else {
      setIsNewUserDisabled(true);
      setNewUserWarning('User data is invalid');
    }
  };

  // Validate data whenever newUser changes
  useEffect(() => {
    if (isNewUser) {
      validateData();
    }
  }, [newUser]);

  const handleLoginEnterPress = (e) => {
    if (e.key === 'Enter') {
      handleLogin();
    }
  };

  if (!isAuthUser) {
    return (<>
      <h1>List of users</h1>
      <p>Please, sign in. (admin, admin)</p>
      <div className={classes.button} onClick={() => handleLogin()}>Sign in</div>

      <div className={classes.loginRow}>
        <div>
          <input
            ref={loginInputRef}
            placeholder='Username'
            type="text"
            value={username}
            onChange={(e) => {
              setUsername(e.target.value);
            }}
          />
        </div>
        <div>
          <input
            placeholder='Password'
            type="text"
            value={password}
            onChange={(e) => {
              setPassword(e.target.value);
            }}
            onKeyDown={handleLoginEnterPress}
          />
        </div>
      </div>
    </>)
  } else {

    return (
      <>
        <h1>List of users</h1>
        <p>&nbsp; {newUserWarning}</p>
        <div className={isNewUser ? classes.dNone : classes.button} 
             onClick={() => handleNewUser(true)}>
                <span className={classes.active}>+</span> Create user
        </div>
        <div className={isNewUser ? classes.button : classes.dNone} 
             onClick={() => handleNewUser(false)}>
                <span className={classes.active}>✕</span> Cancel
        </div>
        <div className={combinedClasses} 
             onClick={() => isNewUserDisabled ? null : createNewUser()} 
             disabled={isNewUserDisabled}>
                <span className={classes.active}>👍</span> Create
        </div>

        <div className={isNewUser ? classes.usersListUserRow : classes.dNone}>
          <div>
            <input
              placeholder='Username'
              type="text"
              value={newUser.username}
              onChange={(e) => {
                handleNewUserUpdate('username', e.target.value);
              }}
            />
          </div>
          <div>
            <input
              placeholder='eMail'
              type="text"
              value={newUser.email}
              onChange={(e) => {
                handleNewUserUpdate('email', e.target.value);
              }}
            />
          </div>
          <div>
            <input
              placeholder='Age'
              type="text"
              value={newUser.age}
              size="5"
              onChange={(e) => {
                handleNewUserUpdate('age', e.target.value);
              }}
            />
          </div>
          <div>

            <input
              type="text"
              value={newUser.country}
              onChange={(e) => {
                handleNewUserUpdate('country', e.target.value);
              }}
              list={`countryList-newUser`}
              placeholder="Enter a country name"
            />
            <datalist id={`countryList-newUser`}>
              {handleFilterCountries(newUser.country).map((country) => (
                <option key={country.code} value={country.name}>
                  {country.name}
                </option>
              ))}
            </datalist>
          </div>
        </div>
        <div className={classes.usersListConatainer}>
          <div className={classes.usersListUserRow}>
            <div>
              Username
              <span className={activeSort === 'username' && sortOrder === 'asc' ? classes.active : classes.inactive} 
                    onClick={() => handleSort('username', 'asc')}>⇑
              </span>
              <span className={activeSort === 'username' && sortOrder === 'dsc' ? classes.active : classes.inactive} 
                    onClick={() => handleSort('username', 'dsc')}>⇓
              </span>
            </div>
            <div>
              eMail
              <span className={activeSort === 'email' && sortOrder === 'asc' ? classes.active : classes.inactive} 
                    onClick={() => handleSort('email', 'asc')}>⇑
              </span>
              <span className={activeSort === 'email' && sortOrder === 'dsc' ? classes.active : classes.inactive} 
                    onClick={() => handleSort('email', 'dsc')}>⇓
              </span>
            </div>
            <div>
              Age
              <span className={activeSort === 'age' && sortOrder === 'asc' ? classes.active : classes.inactive} 
                    onClick={() => handleSort('age', 'asc')}>⇑
              </span>
              <span className={activeSort === 'age' && sortOrder === 'dsc' ? classes.active : classes.inactive} 
                    onClick={() => handleSort('age', 'dsc')}>⇓
              </span>
            </div>
            <div>
              Country
              <span className={activeSort === 'country' && sortOrder === 'asc' ? classes.active : classes.inactive} 
                    onClick={() => handleSort('country', 'asc')}>⇑
              </span>
              <span className={activeSort === 'country' && sortOrder === 'dsc' ? classes.active : classes.inactive} 
                    onClick={() => handleSort('country', 'dsc')}>⇓
              </span>
            </div>
          </div>
          {usersList.map((user, index) => (
            <div className={classes.usersListUserRow} key={index} id={`ur-${index}`}>
              <div>
                <input
                  placeholder='Username'
                  type="text"
                  value={user.username}
                  onChange={(e) => {
                    handleUpdate(index, 'username', e.target.value);
                  }}
                />
              </div>
              <div>
                <input
                  placeholder='eMail'
                  type="text"
                  value={user.email}
                  onChange={(e) => {
                    handleUpdate(index, 'email', e.target.value);
                  }}
                />
              </div>
              <div>
                <input
                  placeholder='Age'
                  type="text"
                  value={user.age}
                  size="5"
                  onChange={(e) => {
                    handleUpdate(index, 'age', e.target.value);
                  }}
                />
              </div>
              <div>

                <input
                  type="text"
                  value={user.country}
                  onChange={(e) => {
                    handleUpdate(index, 'country', e.target.value);
                  }}
                  list={`countryList-${index}`}
                  placeholder="Enter a country name"
                />
                <datalist id={`countryList-${index}`}>
                  {handleFilterCountries(user.country).map((country) => (
                    <option key={country.code} value={country.name}>
                      {country.name}
                    </option>
                  ))}
                </datalist>
              </div>
            </div>
          ))}
        </div>

      </>
    )
  }
}

export default App
